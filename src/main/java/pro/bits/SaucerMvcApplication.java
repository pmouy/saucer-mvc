/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.xhtmlrenderer.pdf.ITextRenderer;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;


@SpringBootApplication
@EnableEurekaClient
public class SaucerMvcApplication {
	
	private final String FONTS_DIR = "fonts";
	
	private Logger logger = LoggerFactory.getLogger(SaucerMvcApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SaucerMvcApplication.class, args);
	}
	
	@Bean
	@Scope("prototype")
	ITextRenderer pdfRenderer(){
	    ITextRenderer renderer = new ITextRenderer();
	    try {
			File folder = new File(FONTS_DIR);
			File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  renderer.getFontResolver().addFont(
		    			  FONTS_DIR + File.separator + listOfFiles[i].getName(),
		    			  BaseFont.IDENTITY_H, 
		    			  BaseFont.EMBEDDED);
		      }
		    }
		} catch (DocumentException | IOException e) {
			
			logger.error(e.getMessage());
			if (logger.isDebugEnabled()){
				e.printStackTrace();
			}
		}
	    return renderer;
	}

}
