/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.ObjectMapper;

import pro.bits.model.RestErrorResponse;

@Controller
@Scope("prototype")
public class MainController {
	
	private Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	ITextRenderer renderer;
	
	ObjectMapper mapper = new ObjectMapper();
	
	private final String TEMP_DIR = "temp";
	
    @RequestMapping(value="/report/pdf", method=RequestMethod.POST)
    public void generatePDFFromHTML (@RequestParam(value="source") String html, HttpServletResponse response)
	{
    	
    	File pdfReportFile = null;
    	File htmlReportFile = null;
    	
    	try{
    		
    		if (html != null){
    			logger.debug("Source Base64-encoded HTML: " + html);
    		}else{
    			throw new Exception("A source html must be provided");
    		}
    		
    		
    		// Generate the random ID for this request
    		String id = UUID.randomUUID().toString();
    		String pdfReport = TEMP_DIR + File.separator + id +".pdf";
    		String htmlReport = TEMP_DIR + File.separator + id +".html";
    		
    		// Decode the base64-encoded html and store the result in the file
    		byte[] htmlReportBytes = Base64.getDecoder().decode(html);
    		htmlReportFile = new File(htmlReport);
    		FileUtils.writeByteArrayToFile(htmlReportFile, htmlReportBytes);  		
	        
    		// Prepare the PDF output file
	        pdfReportFile = new File(pdfReport);
	        OutputStream os = new FileOutputStream(pdfReport);
	        
	        // Render the PDF from the source HTML
	        renderer.setDocument(htmlReportFile.toURI().toURL().toString());      
	        renderer.layout();
	        renderer.createPDF(os);        
	        
	        // Prepare the server HTTP response
	        InputStream is = new FileInputStream(pdfReportFile);
	  		response.setContentType("application/pdf");
	  		IOUtils.copy(is, response.getOutputStream());  
	  		response.setStatus(200);

	        
	        // Clean the house
	        is.close();
	        os.close();
	        if (pdfReportFile != null) pdfReportFile.delete();
	        if (htmlReportFile != null) htmlReportFile.delete();
	        
	        // Send the response
	        response.flushBuffer();
	        
    		
        }catch (Exception e){ 
        	
	        if (pdfReportFile != null) pdfReportFile.delete();
	        if (htmlReportFile != null) htmlReportFile.delete();
     	    if (logger.isDebugEnabled()){
     		   e.printStackTrace();
     	    }
        	
     	    logger.error(e.getMessage());
        	RestErrorResponse error = new RestErrorResponse();
        	error.setErrorMessage(e.getMessage());
        	response.setStatus(500);
        	
        	try {
				IOUtils.copy(new ByteArrayInputStream(this.mapper.writeValueAsBytes(error)), response.getOutputStream());
				response.flushBuffer();
			} catch (IOException i) {
					// Do nothing
			}
        }
	}

}
