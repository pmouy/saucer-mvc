/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits.config;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import pro.bits.security.StatelessAuthenticationFilter;
import pro.bits.security.TokenAuthenticationService;
import pro.bits.security.UserService;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	 
    private final UserService userService;
    private TokenAuthenticationService tokenAuthenticationService;
    
    @Value("${auth.jwt.key}")
    String jwtKey;
    
    public SpringSecurityConfig() {
        super(true);
        this.userService = new UserService();
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/manage/**");
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
		        .servletApi().and()
                .authorizeRequests()
                .antMatchers("/manage/**").permitAll() // Actuator
                
                // All requests need to be authenticated
                .anyRequest().authenticated().and()
 
                // Custom Token based authentication based on the header previously given to the client
                .addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class);
    }
 
    @PostConstruct
	private void loadJwtKey(){
		
		tokenAuthenticationService = new TokenAuthenticationService(this.jwtKey, this.userService);
	}
	    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(new BCryptPasswordEncoder());
    }
 
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
 
    @Bean
    @Override
    public UserService userDetailsService() {
        return userService;
    }
 
    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        return tokenAuthenticationService;
    }
}
