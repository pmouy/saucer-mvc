package pro.bits.config;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TomcatConfiguration {

	// Taken(ish) from: https://stackoverflow.com/questions/31461444/ https://stackoverflow.com/questions/33232849/
	@Bean
	public EmbeddedServletContainerFactory servletContainerFactory() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
	    factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
	    	@Override
    		public void customize(Connector connector) {
    			// Unlimited post for html conversion
    			connector.setMaxPostSize(-1);
			}
    	});
	    
	    return factory;
	}
}
