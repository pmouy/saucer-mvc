/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserService implements org.springframework.security.core.userdetails.UserDetailsService{
	 
 
    public final User loadUserByUsername(String username, String issuer, long emitTime, long expiryTime, String audience) throws UsernameNotFoundException {
        
    	
    	TokenManager tokenManager = TokenManager.getInstance();
    	
    	final User user;    
    	
    	if (username != null) {
    		
			if (tokenManager.isTokenValid(issuer)){ 
	
            		List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
                	List<String> items = Arrays.asList(audience.split("\\s*,\\s*"));
                	if (items.size() > 0){
                    	for (String item : items){
                    		if (!item.equals("")){
                    			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(item);
                        		authorities.add(authority);
                    		}
                    		
                    	}
                	}
            		user = new User(username, "N/A", authorities); // No password	
            		tokenManager.addToken(issuer, expiryTime); // Finally add token to pool
            					
    		}else{
    			throw new UsernameNotFoundException("The token has already been used");
    		}
    	}else{
    		throw new UsernameNotFoundException("Invalid user token");
    	}
    	
        return user;
    }

	@Override
	public UserDetails loadUserByUsername(String arg0)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
}
