/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits.security;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class TokenManager {
	
	private HashMap<String, Long> tokens;
	
	private static TokenManager instance = new TokenManager();
	
	private TokenManager(){
		// Initiate token array
		this.tokens = new HashMap<String,Long>();
	}
	
   public static TokenManager getInstance(){
      return instance;
   }
	
	public void addToken(String tokenId, Long expiryTime){
		this.tokens.put(tokenId, expiryTime);
	}
	
	public boolean isTokenValid(String token){
		
		if (this.tokens.containsKey(token)){
			return false;
		}else{
			return true;
		}
	}
	
	public void clearTokens(){
		
		// use the removal list to avoid concurrent modification exceptions
		ArrayList<String> removalList = new ArrayList<String>();
		
		for( String key : this.tokens.keySet() ) {
	        if (this.tokens.get(key) < System.currentTimeMillis()){ // if token has expired ...
	        	removalList.add(key);
	        }
	    }
	    
	    // now delete them from the hash map
	    for(String key : removalList) {
	    	this.tokens.remove(key);
	    }
		
		
	}
	

}
