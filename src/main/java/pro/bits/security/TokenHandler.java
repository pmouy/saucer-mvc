/*
 *  SaucerMVC - Spring Boot MVC application that converts HTML to PDF
 *  Copyright (C) 2016  Pierre-Alain Mouy
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pro.bits.security;

import org.springframework.security.core.userdetails.User;
import io.jsonwebtoken.Jwts;

public final class TokenHandler {
	 
    private final String secret;
    private final UserService userService;
 
    public TokenHandler(String secret, UserService userService) {
        this.secret = secret;
        this.userService = userService;
    }
 
    public User parseUserFromToken(String token) {
    	
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        
        String issuer = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getIssuer();
        
        long issueTime = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getIssuedAt().getTime();
        
        long expiryTime = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getExpiration().getTime();
        
        String audience = Jwts.parser()
		        .setSigningKey(secret)
		        .parseClaimsJws(token)
		        .getBody()
		        .getAudience();
        
        
        
        return userService.loadUserByUsername(username, issuer, issueTime, expiryTime, audience);
    }
}
