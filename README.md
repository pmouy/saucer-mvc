#README#

## Description

The SaucerMVC application is based on Spring Boot and enables the generation of PDF files based on XHTML input.

The ''source'' parameter on the endpoint /report/pdf is the original HTML and must be encoded in Base64.

The Authentication mechanism used is JWT-token based


## License
GNU Affero General Public License